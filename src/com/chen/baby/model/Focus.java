package com.chen.baby.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Focus {
	private int focusId;
	private int userId;
	private int otherParty;
	private int permission;
	private int agree;
	private Date createTime;
	private Date agreeTime;
	@Id
	@GeneratedValue
	public int getFocusId() {
		return focusId;
	}
	public void setFocusId(int focusId) {
		this.focusId = focusId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getOtherParty() {
		return otherParty;
	}
	public void setOtherParty(int otherParty) {
		this.otherParty = otherParty;
	}
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public int getAgree() {
		return agree;
	}
	public void setAgree(int agree) {
		this.agree = agree;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getAgreeTime() {
		return agreeTime;
	}
	public void setAgreeTime(Date agreeTime) {
		this.agreeTime = agreeTime;
	}
	
}
