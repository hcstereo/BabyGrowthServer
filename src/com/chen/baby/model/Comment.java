package com.chen.baby.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Comment {
	private int commentId;
	private int fromUserId;
	private int recordId;
	private String createTime;
	private int hasView;
	private String body;
	private String nickName;
	private String avatar;
	@Id
	@GeneratedValue
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public int getFromUserId() {
		return fromUserId;
	}
	public void setFromUserId(int fromUserId) {
		this.fromUserId = fromUserId;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getHasView() {
		return hasView;
	}
	public void setHasView(int hasView) {
		this.hasView = hasView;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
//	public String getNickName() {
//		return nickName;
//	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
//	public String getAvatar() {
//		return avatar;
//	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
}
