package com.chen.baby.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LogTool {
	/**
	 * Get log
	 * @param T
	 * @return
	 */
	public static Log getLog(Class<?> T) {
		Log log = LogFactory.getLog(T);
		return log;
	}
	
}