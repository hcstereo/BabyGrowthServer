package com.chen.baby.action;

import java.util.List;

import com.chen.baby.dao.CommentDao;
import com.chen.baby.model.Comment;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class CommentAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4100L;
	private int recordId;
	private CommentDao commentDao;
	private String commentsJson;
	private String errorCode;
	private static final String ERROR_CODE_FETCH_SUCCESS = "4100";
	private static final String ERROR_CODE_FETCH_NO_RESULT = "4101";
	private static final String FETCH_COMMENT_SUCCESS = "fetch_comment_success";
	/**
	 * fetch Comment Byte recordid
	 * @return
	 */
	public String commentByRecord(){
		Gson gson = new Gson();
		List<Comment> comments = commentDao.getCommentByRecordId(recordId);
		commentsJson = gson.toJson(comments);
		if(comments.size() > 0){
			errorCode = ERROR_CODE_FETCH_SUCCESS;
		}else{
			errorCode = ERROR_CODE_FETCH_NO_RESULT;
		}
		return FETCH_COMMENT_SUCCESS;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	public String getCommentsJson() {
		return commentsJson;
	}

	public void setCommentsJson(String commentsJson) {
		this.commentsJson = commentsJson;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}
