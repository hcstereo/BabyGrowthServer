package com.chen.baby.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chen.baby.dao.UserDao;
import com.chen.baby.model.User;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private Log logTool = LogFactory.getLog(UserAction.class);
	private Gson gson = new Gson();
	private String nickName;
	private String password;
	private String userJson;
	private String errorCode;
	private int userId;
	
	private final static String LOGIN_SUCCESS="login_success";
	private final static String LOGIN_FAIL_USER_NOT_EXIST="user_not_exist";
	private final static String LOGIN_FAIL_USER_WRONG_PWD="user_wrong_pwd";
	
	private final static String ERROR_CODE_lOGIN_SUCCESS="1100";
	private final static String ERROR_CODE_LOGIN_FAIL_USER_NOT_EXIST="1101";
	private final static String ERROR_CODE_LOGIN_FAIL_USER_WRONG_PWD="1102";
	
	private final static String FETCH_USERINFO_SUCCESS="fetch_userinfo_success";
	private final static String ERROR_CODE_FETCH_USERINFO_SUCCESS = "1103";
	private final static String ERROR_CODE_FETCH_USER_BYID_SUCCESS = "1104";
	
	private UserDao userDao;
	
	/**
	 * user login interface
	 * @return
	 */	
	public String login(){
		logTool.info("user named "+nickName+" is trying to login......");
		User user=new User();
		user = userDao.getUserByName(nickName);
		if(user == null){
			errorCode = ERROR_CODE_LOGIN_FAIL_USER_NOT_EXIST;
			return LOGIN_FAIL_USER_NOT_EXIST;
		}
		if(user.getPassword().equals(password)){
			userJson = gson.toJson(user);
			errorCode = ERROR_CODE_lOGIN_SUCCESS;
			return LOGIN_SUCCESS;
		}else{
			errorCode = ERROR_CODE_LOGIN_FAIL_USER_WRONG_PWD;
			return LOGIN_FAIL_USER_WRONG_PWD;
		}
	}
	/**
	 * 根据用户名获取用户信息
	 * @return
	 */
	public String info(){
		logTool.info("user named "+nickName+" is trying to fetch info......");
		User user = userDao.getUserByName(nickName);
		userJson = gson.toJson(user);
		errorCode = ERROR_CODE_FETCH_USERINFO_SUCCESS;
		return FETCH_USERINFO_SUCCESS;
	}
	/**
	 * 根据用户名获取用户信息
	 * @return
	 */
	public String infoId(){
		logTool.info("user id =  "+userId+" is trying to fetch info......");
		User user = userDao.getUserById(userId);
		userJson = gson.toJson(user);
		errorCode = ERROR_CODE_FETCH_USER_BYID_SUCCESS;
		logTool.info("userJson id =  "+userJson);
		return FETCH_USERINFO_SUCCESS;
	}
	
	/*----------------------------getter setter-----------------------------*/
	
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserJson() {
		return userJson;
	}
	public void setUserJson(String userJson) {
		this.userJson = userJson;
	}
	public UserDao getUserDao() {
		return userDao;
	}
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
