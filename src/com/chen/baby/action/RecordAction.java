package com.chen.baby.action;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chen.baby.dao.CommentDao;
import com.chen.baby.dao.RecordDao;
import com.chen.baby.dao.UserDao;
import com.chen.baby.model.Record;
import com.chen.baby.model.User;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

/**
 * fetch用户发表的状态
 * @author huachongchen
 *
 */
public class RecordAction extends ActionSupport{
	private Log logTool = LogFactory.getLog(UserAction.class);
	/**
	 */
	private static final long serialVersionUID = 10000L;
	
	private static final String ERROR_CODE_FETCH_SUCCESS = "2100";
	private static final String ERROR_CODE_FETCH_NO_RECORD = "2101";
	private static final String FETCH_RECORD_SUCCESS = "fetch_record_success";
	
	private int userId;
	private String nickName;
	private String errorCode;
	private String recordsJson;
	private Gson gson = new Gson();
	private UserDao userDao;
	private RecordDao recordDao;
	private CommentDao commentDao;
	public String record(){
		User user = userDao.getUserByName(nickName);
		logTool.info("user="+user);
		logTool.info("userId="+user.getUserId());
		logTool.info("recodrDao="+recordDao);
		List<Record> records = recordDao.fetchRecord(user.getUserId());
		recordsJson = gson.toJson(records);
		if(records == null || records.size() == 0){
			errorCode = ERROR_CODE_FETCH_NO_RECORD;
		}else{
			errorCode = ERROR_CODE_FETCH_SUCCESS;
		}
		return FETCH_RECORD_SUCCESS;
	}
	public String recordById(){
		List<Record> records = recordDao.fetchRecord(userId);
		for(Record record:records){
			record.setComments(commentDao.getCommentByRecordId(record.getRecordId()));
		}
		recordsJson = gson.toJson(records);
		if(records == null || records.size() == 0){
			errorCode = ERROR_CODE_FETCH_NO_RECORD;
		}else{
			errorCode = ERROR_CODE_FETCH_SUCCESS;
		}
		return FETCH_RECORD_SUCCESS;
	}

	
	/*----------------------------getter setter-----------------------------*/
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getRecordsJson() {
		return recordsJson;
	}

	public void setRecordsJson(String recordsJson) {
		this.recordsJson = recordsJson;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public RecordDao getRecordDao() {
		return recordDao;
	}

	public void setRecordDao(RecordDao recordDao) {
		this.recordDao = recordDao;
	}



	public int getUserId() {
		return userId;
	}

	public CommentDao getCommentDao() {
		return commentDao;
	}
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
