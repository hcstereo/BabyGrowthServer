package com.chen.baby.action;

import com.chen.baby.dao.BabyDao;
import com.chen.baby.model.Baby;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class BabyAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String TAG = BabyAction.class.getSimpleName();
	private int babyId;
	private BabyDao babyDao;
	private String babyJson;
	private String errorCode;
	private static final String ERROR_CODE_FETCH_SUCCESS = "3100";
	private static final String ERROR_CODE_FETCH_NO_RESULT = "3101";
	private static final String FETCH_BABY_SUCCESS = "fetch_baby_success";
	
	public String babyById(){
		Baby baby = babyDao.fetchBaby(babyId);
		babyJson = new Gson().toJson(baby);
		if(baby == null || baby.getBabyId() == 0){
			errorCode = ERROR_CODE_FETCH_NO_RESULT;
		}else{
			errorCode = ERROR_CODE_FETCH_SUCCESS;
		}
		return FETCH_BABY_SUCCESS;
	}
	
	/*----------------------------------------------------------*/

	public int getBabyId() {
		return babyId;
	}

	public void setBabyId(int babyId) {
		this.babyId = babyId;
	}

	public BabyDao getBabyDao() {
		return babyDao;
	}

	public void setBabyDao(BabyDao babyDao) {
		this.babyDao = babyDao;
	}

	public String getBabyJson() {
		return babyJson;
	}

	public void setBabyJson(String babyJson) {
		this.babyJson = babyJson;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
}
