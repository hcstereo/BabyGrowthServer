package com.chen.baby.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.chen.baby.dao.PraiseDao;
import com.chen.baby.model.Praise;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class PraiseAction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int userId;
	private int recordId;
	private int expression;
	private boolean isCancel;
	private String praiseJson;
	private String errorCode;
	private PraiseDao praiseDao;
	private static final String PRAISE_SUCCESS = "praise_success";
	private static final String ERROR_CODE_PRAISE_SUCCESS = "5100";
	private Log logTool = LogFactory.getLog(PraiseAction.class);
	/**
	 * 点赞
	 * @return
	 */
	public String praise(){
		Praise p = praiseDao.praise(userId, recordId, expression);
		praiseJson = new Gson().toJson(p);
		errorCode = ERROR_CODE_PRAISE_SUCCESS;
		logTool.info(praiseJson);
		return PRAISE_SUCCESS;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRecordId() {
		return recordId;
	}
	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}
	public boolean isCancel() {
		return isCancel;
	}
	public void setCancel(boolean isCancel) {
		this.isCancel = isCancel;
	}

	public String getPraiseJson() {
		return praiseJson;
	}

	public void setPraiseJson(String praiseJson) {
		this.praiseJson = praiseJson;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public int getExpression() {
		return expression;
	}

	public void setExpression(int expression) {
		this.expression = expression;
	}

	public PraiseDao getPraiseDao() {
		return praiseDao;
	}

	public void setPraiseDao(PraiseDao praiseDao) {
		this.praiseDao = praiseDao;
	}
	
}
