package com.chen.baby.dao;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;
/**
 * dao基类，用于获得hibernateTemplate
 * @author huachongchen
 *
 */
public class BabyBaseDao {
	private Log logTool = LogFactory.getLog(BabyBaseDao.class);
	private HibernateTemplate hibernateTemplate;
	protected Session session;
	protected Transaction transaction; //hiberante 事务
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	@Resource(name = "hibernateTemplate")
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	public SessionFactory getSessionFactory() {
		return this.getHibernateTemplate().getSessionFactory();
	}
	protected void beginTransaction() {
		session = getSessionFactory().openSession();
		logTool.info("openSession");
		transaction = session.beginTransaction();
		logTool.info("beginTransaction");
	}
	protected void endTransaction(boolean commit) {
		if(commit){
			transaction.commit();
		}else{
			transaction.rollback();
		}
		session.close();
	}
}
