package com.chen.baby.dao;

import org.springframework.stereotype.Component;

import com.chen.baby.model.Baby;

@Component("babyDao")
public class BabyDao extends BabyBaseDao{
	public Baby fetchBaby(int babyId){
		beginTransaction();
		String query = "select * from baby where babyId="+babyId;
		Baby baby = (Baby) session.createSQLQuery(query).addEntity(Baby.class).uniqueResult();
		endTransaction(true);
		return baby;
	}
}
