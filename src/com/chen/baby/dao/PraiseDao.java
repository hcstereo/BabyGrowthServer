package com.chen.baby.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.chen.baby.model.Praise;
@Component("praiseDao")
public class PraiseDao extends BabyBaseDao{
	public Praise praise(int userId,int recordId,int expression){
		if(getPraise(userId,recordId) == null){
			beginTransaction();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String query = "insert into praise (userId,recordId,createTime,expression,hasView) values("+userId+","+recordId+",'"+format.format(new Date())+"',"+expression+",0)";
			session.createSQLQuery(query).executeUpdate();
			endTransaction(true);
		}
		Praise ppp = getPraise(userId,recordId);
		ppp.setAvatar(getAvatar(userId));
		ppp.setNickName(getNickName(userId));
		return ppp;
	}
	public Praise getPraise(int userId,int recordId){
		beginTransaction();
		String query = "select * from praise where userId="+userId+" and recordId="+recordId+" limit 1";
		List<Praise> praises = session.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Praise.class)).list();
		endTransaction(true);
		return praises.size()==0?null:praises.get(0);
	}
	public String getAvatar(int userId){
		beginTransaction();
		String query = "select avatar from user where userId="+userId;
		String avatar = (String) session.createSQLQuery(query).uniqueResult();
		endTransaction(true);
		return avatar;
	}
	public String getNickName(int userId){
		beginTransaction();
		String query = "select nickName from user where userId="+userId;
		String avatar = (String) session.createSQLQuery(query).uniqueResult();
		endTransaction(true);
		return avatar;
	}
}
