package com.chen.baby.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.chen.baby.action.UserAction;
import com.chen.baby.model.User;

/**
 * userdao deal request about user table
 * @author huachongchen
 *
 */
@Component("userDao")
public class UserDao extends BabyBaseDao{
	private Log logTool = LogFactory.getLog(UserDao.class);
	
	public User getUserByName(String nickName){
		beginTransaction();
		String query="select * from user where nickName = '"+nickName+"'";
		User user = (User) session.createSQLQuery(query).addEntity(User.class).uniqueResult();
		endTransaction(true);
		return user;
	}
	
	public User getUserById(int  userId){
		logTool.info("begin fetch user userId="+userId);
		beginTransaction();
		String query="select * from user where userId = "+userId;
		logTool.info("query="+query);
		User user = (User) session.createSQLQuery(query).addEntity(User.class).uniqueResult();
		logTool.info("user="+user.getNickName());
		endTransaction(true);
		return user;
	}
	
	public boolean addUser(User user){
		beginTransaction();
		if(isUserExist(user.getNickName())){
			endTransaction(true);
			return false;
		}
		session.save(user);
		endTransaction(true);
		return true;
	}
	public boolean isUserExist(String nickName) {
		beginTransaction();
		String query = "select * from user where nickName="+nickName;
		User user = (User) session.createQuery(query).uniqueResult();
		endTransaction(true);
		return user != null;
	}
}
