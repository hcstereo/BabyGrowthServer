package com.chen.baby.dao;

import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import com.chen.baby.model.Comment;
import com.chen.baby.model.User;
@Component("commentDao")
public class CommentDao extends BabyBaseDao{
	private UserDao userDao;
	private static final String TAG = CommentDao.class.getSimpleName();
	
	public List<Comment> getCommentByRecordId(int recordId){
		beginTransaction();
		String query = "select * from comment where recordId="+recordId;
		List<Comment> comments = session.createSQLQuery(query).addEntity(Comment.class).list();
		endTransaction(true);
		for(Comment c:comments){
			User user = userDao.getUserById(c.getFromUserId());
			c.setAvatar(user.getAvatar());
			c.setNickName(user.getNickName());
		}
		return comments;
	}
	
	public UserDao getUserDao() {
		return userDao;
	}
	@Resource(name="userDao")
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
