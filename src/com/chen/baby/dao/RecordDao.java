package com.chen.baby.dao;

import java.util.List;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;

import com.chen.baby.model.Focus;
import com.chen.baby.model.Praise;
import com.chen.baby.model.Record;
import com.chen.baby.model.User;

/**
 * userdao deal request about user table
 * @author huachongchen
 *
 */
@Component("recordDao")
public class RecordDao extends BabyBaseDao{
	/**
	 * 根据用户id获取用户状态
	 * @param userId
	 * @return
	 */
	public List<Record> fetchRecord(int userId){
		beginTransaction();
		String query="(select * from record where userId = "+userId+") union (select * from record where userId in (select otherParty from focus where userId = "+userId+"))";
		@SuppressWarnings("unchecked")
		List<Record> records = session.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Record.class)).list();
		endTransaction(true);
		for(Record record : records){
			record.setPraiseSum(getPraiseSum(record.getRecordId()));
			record.setAvatar(getAvatar(record.getUserId()));
			record.setPraises(getPraise(record.getRecordId()));
		}
		return records;
	}
	public int getPraiseSum(int record){
		beginTransaction();
		String query = "select count(*) from praise where recordId="+record;
		int result = ((Number) session.createSQLQuery(query).uniqueResult()).intValue();
		endTransaction(true);
		return result;
	}
	public String getAvatar(int userId){
		beginTransaction();
		String query = "select avatar from user where userId="+userId;
		String avatar = (String) session.createSQLQuery(query).uniqueResult();
		endTransaction(true);
		return avatar;
	}
	public String getNickName(int userId){
		beginTransaction();
		String query = "select nickName from user where userId="+userId;
		String avatar = (String) session.createSQLQuery(query).uniqueResult();
		endTransaction(true);
		return avatar;
	}
	public List<Praise> getPraise(int recordId){
		beginTransaction();
		String query = "select * from praise where recordId="+recordId;
		@SuppressWarnings("unchecked")
		List<Praise> praises = session.createSQLQuery(query).setResultTransformer(Transformers.aliasToBean(Praise.class)).list();
		endTransaction(true);
		for(Praise p:praises){
			p.setAvatar(getAvatar(p.getUserId()));
			p.setNickName(getNickName(p.getUserId()));
		}
		
		return praises;
	}
}
