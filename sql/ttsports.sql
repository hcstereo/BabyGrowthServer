/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : ttsports

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2014-06-24 20:55:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for chatmsg
-- ----------------------------
DROP TABLE IF EXISTS `chatmsg`;
CREATE TABLE `chatmsg` (
  `msgId` int(11) NOT NULL AUTO_INCREMENT,
  `fromUserId` int(11) NOT NULL,
  `toUserId` int(11) NOT NULL,
  `msgContent` text,
  `msgType` int(11) DEFAULT NULL,
  `msgStateId` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  PRIMARY KEY (`msgId`),
  KEY `toUserId` (`toUserId`),
  KEY `fromUserId` (`fromUserId`),
  KEY `msgType` (`msgType`),
  CONSTRAINT `chatmsg_ibfk_3` FOREIGN KEY (`msgType`) REFERENCES `msgtype` (`typeId`),
  CONSTRAINT `chatmsg_ibfk_1` FOREIGN KEY (`fromUserId`) REFERENCES `user` (`userId`),
  CONSTRAINT `chatmsg_ibfk_2` FOREIGN KEY (`fromUserId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chatmsg
-- ----------------------------

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `relId` int(11) NOT NULL AUTO_INCREMENT,
  `hostId` int(11) DEFAULT NULL,
  `friendsId` int(11) DEFAULT NULL,
  `relState` int(11) DEFAULT NULL,
  PRIMARY KEY (`relId`),
  KEY `hostId` (`hostId`),
  KEY `friendsId` (`friendsId`),
  CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`friendsId`) REFERENCES `user` (`userId`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`hostId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES ('1', '1', '2', '1');
INSERT INTO `friends` VALUES ('2', '1', '3', '1');
INSERT INTO `friends` VALUES ('3', '1', '4', '1');
INSERT INTO `friends` VALUES ('4', '1', '5', '1');
INSERT INTO `friends` VALUES ('5', '1', '6', '1');
INSERT INTO `friends` VALUES ('6', '1', '11', '1');
INSERT INTO `friends` VALUES ('7', '1', '12', '1');
INSERT INTO `friends` VALUES ('8', '1', '13', '1');
INSERT INTO `friends` VALUES ('9', '1', '14', '1');
INSERT INTO `friends` VALUES ('10', '1', '15', '1');
INSERT INTO `friends` VALUES ('11', '1', '16', '1');
INSERT INTO `friends` VALUES ('12', '1', '17', '1');
INSERT INTO `friends` VALUES ('13', '1', '18', '1');
INSERT INTO `friends` VALUES ('14', '1', '19', '1');
INSERT INTO `friends` VALUES ('15', '1', '20', '1');

-- ----------------------------
-- Table structure for msgtype
-- ----------------------------
DROP TABLE IF EXISTS `msgtype`;
CREATE TABLE `msgtype` (
  `typeId` int(11) NOT NULL AUTO_INCREMENT,
  `typeName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of msgtype
-- ----------------------------

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `statusId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `calories` double DEFAULT NULL,
  `runTime` datetime DEFAULT NULL,
  PRIMARY KEY (`statusId`),
  KEY `userId` (`userId`),
  CONSTRAINT `status_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of status
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `gender` varchar(5) DEFAULT NULL,
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `hobby` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `password` varchar(30) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `regTime` datetime DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '猪', '男', '170', '67', '篮球', '/ttsports/file/1/avastar/chen.jpg', '1', '18363125675', '2014-06-22 18:03:03', '小子，要你好看');
INSERT INTO `user` VALUES ('2', '小狗', '男', '2', '2', '2', '/ttsports/file/1/avastar/chen1.jpg', '2', '1', '2014-06-23 14:06:38', '无所谓了，算了');
INSERT INTO `user` VALUES ('3', '小猫', '女', '3', '3', '3', '/ttsports/file/1/avastar/chen2.jpg', '3', '3', '2014-06-23 14:07:00', '今天适合打球');
INSERT INTO `user` VALUES ('4', '小鸡', '女', '4', '4', '4', '/ttsports/file/1/avastar/chen3.jpg', '4', '4', '2014-06-23 17:38:27', '走，走，走');
INSERT INTO `user` VALUES ('5', '唯爱你', '男', '180', '80', '足球 电影', '/ttsports/file/1/avastar/chen5.jpg', '5', '18856758865', '2014-06-23 22:08:06', '唯一爱的就是你');
INSERT INTO `user` VALUES ('6', '24k', '男', '175', '67', '足球', '/ttsports/file/1/avastar/chen4.jpg', '6', '13565331275', '2014-06-23 22:13:11', '无签名');
INSERT INTO `user` VALUES ('11', '而且', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:03:18', '我还没有签名');
INSERT INTO `user` VALUES ('12', '毕竟', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:05:41', '我还没有签名');
INSERT INTO `user` VALUES ('13', '毕竟1', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:09:03', '我还没有签名');
INSERT INTO `user` VALUES ('14', '爱哭', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:12:05', '我还没有签名');
INSERT INTO `user` VALUES ('15', '注意', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', '1', null, '2014-06-24 00:36:59', '我还没有签名');
INSERT INTO `user` VALUES ('16', '啊呸', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:41:47', '我还没有签名');
INSERT INTO `user` VALUES ('17', '只要有你', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', '1', null, '2014-06-24 00:42:53', '我还没有签名');
INSERT INTO `user` VALUES ('18', '最', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:45:44', '我还没有签名');
INSERT INTO `user` VALUES ('19', '最美', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:46:04', '我还没有签名');
INSERT INTO `user` VALUES ('20', '十二生肖', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:48:27', '我还没有签名');
INSERT INTO `user` VALUES ('21', '你来试试', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', 'q', null, '2014-06-24 00:49:54', '我还没有签名');
INSERT INTO `user` VALUES ('22', '陈唐浩', '男', '175', '0', '没有兴趣', '/ttsports/file/1/avastar/chen.jpg', '1', null, '2014-06-24 00:53:47', '我还没有签名');
INSERT INTO `user` VALUES ('23', '陈艳超', '男', '175', '0', '没有兴趣', '', 'q', null, '2014-06-24 00:55:12', '我还没有签名');
INSERT INTO `user` VALUES ('24', '王亚东', '男', '175', '0', '没有兴趣', '', 'q', null, '2014-06-24 00:55:46', '我还没有签名');
INSERT INTO `user` VALUES ('25', '最美的太阳', '男', '175', '0', '没有兴趣', '', 'q', '', '2014-06-24 00:58:33', '我还没有签名');
INSERT INTO `user` VALUES ('26', '丁晓娜', '男', '175', '0', '没有兴趣', '', 'q', '', '2014-06-24 01:01:32', '我还没有签名');
